package com.easy.bar.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static java.util.Collections.*;

/**
 * Created by Adam Krysiak on 23.04.17.
 */

@Service
public class TokenAuthServiceImpl implements TokenAuthService {

    private static final String SIGN_KEY = "sadhgfisdiusadhs";
    private static final int TOKEN_EXPIRATION_DAYS = 7;
    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Override
    public void addAuthentication(HttpServletResponse response, String username) {
        response.addHeader(AUTHORIZATION_HEADER, buidJWT(username));
    }

    @Override
    public Authentication getAuthentication(HttpServletRequest request) {
        String header = request.getHeader(AUTHORIZATION_HEADER);
        return header != null ? getAuthenticationToken(header) : null;
    }

    private UsernamePasswordAuthenticationToken getAuthenticationToken(String header) {
        String user = getUserFromJwts(header);
        return user != null ? new UsernamePasswordAuthenticationToken(user, null, emptyList()) : null;
    }

    private String getUserFromJwts(String header) {
        return Jwts.parser()
                .setSigningKey(SIGN_KEY)
                .parseClaimsJws(header)
                .getBody()
                .getSubject();
    }

    private String buidJWT(String username) {
        return Jwts.builder()
                .setSubject(username)
                .signWith(SignatureAlgorithm.HS256, SIGN_KEY)
                .setExpiration(getLocalDateMovedByWeeks(TOKEN_EXPIRATION_DAYS))
                .compact();
    }

    private Date getLocalDateMovedByWeeks(int weeksToAdd) {
        Instant instant = LocalDate.now()
                .plusDays(weeksToAdd)
                .atStartOfDay(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }
}

