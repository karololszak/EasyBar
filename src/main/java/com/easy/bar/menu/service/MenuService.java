package com.easy.bar.menu.service;

import com.easy.bar.menu.model.Menu;

import java.util.List;

/**
 * Created by lukasz on 5/27/17.
 */
public interface MenuService {
    Menu addMenu(Menu menu);
    void deleteMenu(Integer id);
    Menu updateMenu(Menu menu);
    Menu getMenu(Integer id);
    List<Menu> getMenus();
}
