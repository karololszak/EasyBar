package com.easy.bar.auth;

import lombok.Getter;

/**
 * Created by Adam Krysiak on 23.04.17.
 */
@Getter
public class AuthenticationCredentials {
    String username;
    String password;
}
