package com.easy.bar.user.repository;

import com.easy.bar.user.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

/**
 * Created by Adam Krysiak on 16.03.17.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findById(Long id);

    Optional<User> findByName(String name);
}
