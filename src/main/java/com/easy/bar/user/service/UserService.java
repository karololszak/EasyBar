package com.easy.bar.user.service;


import com.easy.bar.user.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * Created by Adam Krysiak on 18.04.17.
 */

public interface UserService extends UserDetailsService {

    Long saveUser(User user);
    List<User> getAllUsers();

}
