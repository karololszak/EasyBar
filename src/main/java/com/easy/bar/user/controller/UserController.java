package com.easy.bar.user.controller;

import com.easy.bar.user.model.User;
import com.easy.bar.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Adam Krysiak on 16.03.17.
 */
@RestController
@RequestMapping("/api/v1/secured/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> getAll() {
        return userService.getAllUsers();
    }

}
