package com.easy.bar.meal.service;

import com.easy.bar.meal.model.Meal;
import com.easy.bar.meal.repository.MealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lukasz on 5/27/17.
 */
@Service
public class MealServiceImpl implements MealService{

    private final MealRepository mealRepository;

    @Autowired
    public MealServiceImpl(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }

    @Override
    public Meal addMeal(Meal meal) {
        return mealRepository.save(meal);
    }

    @Override
    public void deleteMeal(Integer id) {
        mealRepository.delete(id);
    }

    @Override
    public Meal updateMeal(Meal meal) {
        return mealRepository.save(meal);
    }

    @Override
    public Meal getMeal(Integer id) {
        return mealRepository.findOne(id);
    }

    @Override
    public List<Meal> getMeals() {
        return javaslang.collection.List.ofAll(mealRepository.findAll()).toJavaList();
    }
}
