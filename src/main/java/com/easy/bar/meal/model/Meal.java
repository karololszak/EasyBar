package com.easy.bar.meal.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Generated;
import javax.persistence.*;

/**
 * Created by lukasz on 5/27/17.
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Meal {
    @Id
    @GeneratedValue
    private Integer id;
    private String description;
    private Integer weight;
    private Float price;

    @Enumerated(EnumType.ORDINAL)
    private MealType mealType;
}
