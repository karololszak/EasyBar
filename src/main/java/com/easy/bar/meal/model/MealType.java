package com.easy.bar.meal.model;

/**
 * Created by lukasz on 5/27/17.
 */
public enum MealType {
    DRINK,
    FOOD,
    VEGAN,
}
