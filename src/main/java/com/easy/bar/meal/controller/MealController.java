package com.easy.bar.meal.controller;

import com.easy.bar.meal.model.Meal;
import com.easy.bar.meal.service.MealServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by lukasz on 5/27/17.
 */
@RequestMapping("/meal")
@RestController
public class MealController {

    @Autowired
    private MealServiceImpl mealService;

    @PostMapping
    public Meal addMeal(@RequestBody Meal meal) {
        return mealService.addMeal(meal);
    }

    @PutMapping
    public Meal updateMeal(@RequestBody Meal meal) {
        return mealService.addMeal(meal);
    }

    @GetMapping
    public List<Meal> getMeals() {
        return mealService.getMeals();
    }

    @GetMapping("/{id}")
    public Meal getMeal(@PathVariable Integer id) {
        return mealService.getMeal(id);
    }

    @DeleteMapping("/{id}")
    public void deleteMeal(@PathVariable Integer id) {
        mealService.deleteMeal(id);
    }
}
