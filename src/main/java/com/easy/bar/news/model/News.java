package com.easy.bar.news.model;

import com.easy.bar.user.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Adam Krysiak on 18.04.17.
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class News {
    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    private String title;

    @NotNull
    @Type(type = "text")
    private String text;

    @NotNull
    private Date date;

    @ManyToOne
    private User author;

}
