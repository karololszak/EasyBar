package com.easy.bar.news.service;

import com.easy.bar.news.model.News;
import com.easy.bar.news.repository.NewsRepository;
import javaslang.collection.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class NewsServiceImpl implements NewsService {

    private final NewsRepository newsRepository;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    @Override
    public Integer add(News news) {
        return newsRepository.save(news).getId();
    }

    @Override
    public List<News> getNewsAfter(Date startDate) {
        return newsRepository.findNewsByDateAfter(startDate);
    }

    @Override
    public List<News> getNewsBefore(Date endDate) {
        return newsRepository.findNewsByDateBefore(endDate);
    }

    @Override
    public List<News> getNewsFrom(Date exactDate) {
        return newsRepository.findNewsByDate(exactDate);
    }

    @Override
    public List<News> getAll() {
        return List.ofAll(newsRepository.findAll());
    }

    @Override
    public News save(News news) {
        return newsRepository.save(news);
    }
}
