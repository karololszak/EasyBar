package com.easy.bar.news.controller;

import com.easy.bar.news.model.News;
import com.easy.bar.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Adam Krysiak on 18.04.17.
 */
@RestController
@RequestMapping(path = "/news")
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping
    List<News> getAll() {
        return newsService.getAll().toJavaList();
    }

    @PutMapping
    News update(@RequestParam News toUpdate){
        return newsService.save(toUpdate);
    }

    @PostMapping(path = "/add")
    Integer add(@RequestBody News news) {
        return newsService.add(news);
    }
}
